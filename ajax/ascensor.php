<?php 
session_start();
require_once "../modelos/Ascensor.php";

$ascensor = new Ascensor();

//Datos desde el formulario - Editar ascensor
$idascensor = isset($_POST["idascensor"]) ? limpiarCadena($_POST["idascensor"]) : "";
$idtascensor = isset($_POST["idtascensor"]) ? limpiarCadena($_POST["idtascensor"]) : "";
$marca = isset($_POST["marca"]) ? limpiarCadena($_POST["marca"]) : "";
$modelo = isset($_POST["modelo"]) ? limpiarCadena($_POST["modelo"]) : "";
$ken = isset($_POST["ken"]) ? limpiarCadena($_POST["ken"]) : "";
$pservicio = isset($_POST["pservicio"]) ? limpiarCadena($_POST["pservicio"]) : "";
$gtecnica = isset($_POST["gtecnica"]) ? limpiarCadena($_POST["gtecnica"]) : "";
$valoruf = isset($_POST["valoruf"]) ? limpiarCadena($_POST["valoruf"]) : "";
$valorclp = isset($_POST["valorclp"]) ? limpiarCadena($_POST["valorclp"]) : "";
$paradas = isset($_POST["paradas"]) ? limpiarCadena($_POST["paradas"]) : "";
$capkg = isset($_POST["capkg"]) ? limpiarCadena($_POST["capkg"]) : "";
$capper = isset($_POST["capper"]) ? limpiarCadena($_POST["capper"]) : "";
$velocidad = isset($_POST["velocidad"]) ? limpiarCadena($_POST["velocidad"]) : "";
$dcs = isset($_POST["dcs"]) ? limpiarCadena($_POST["dcs"]) : "";
$elink = isset($_POST["elink"]) ? limpiarCadena($_POST["elink"]) : "";
$codigo = isset($_POST["codigo"]) ? limpiarCadena($_POST["codigo"]) : "";

switch ($_GET["op"]) {
	
            case 'solid_edifid':
            $idcontrato = $_GET["id"]; 
            $rspta=$ascensor->solid_ascid($idcontrato);
            $ascensores = Array();
		while ($reg = $rspta->fetch_object()){
			$ascensores[] = array(
					"0"=>$reg->idascensor,
					"1"=>$reg->nombre.", ".$reg->calle." ".$reg->numero,
					"2"=>$reg->region_ordinal,
					"3"=>$reg->marca,
					"4"=>$reg->modelo
				);
		}

		$results = array(
				"ascensores"=>$ascensores,
				"nascensores"=>count($ascensores)
			);
                
		echo json_encode($results);
		break;


	case 'listarsoid':
	    $rspta=$contrato->listarsolid();
		$data = Array();
		while ($reg = $rspta->fetch_object()){
			$data[] = array(
					"0"=>'<button class="btn btn-info btn-xs" onclick="mostar('.$reg->idcontrato.')" data-tooltip="tooltip" title="Asignar IDs" ><i class="fa fa-hashtag"></i></button>',
					"1"=>$reg->ncontrato,
					"2"=>$reg->fecha,
					"3"=>$reg->region_nombre.' - '.$reg->region_ordinal,
					"4"=>$reg->nedificios,
					"5"=>$reg->nascensores
				);
		}
		$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

		echo json_encode($results);
		break;
        
        case 'InsertarIds':
            $idascensor = $_GET["id"]; 
            $iduser=$_SESSION['iduser'];
            $idascensores = $_POST['idascensor'];
            $codigos = $_POST['codigo'];
            foreach( $idascensores as $index => $idascensor ) {
                $ascensor->InsertarIds($idascensor, $codigos[$index], $iduser);
            }
            echo "Identificadores registrados";

        break;
        
        case 'listarascensor':
        $rspta = $ascensor->listar();
        $data = Array();
        while ($reg = $rspta->fetch_object()) {
            $data[] = array(
                "0" => '<button class="btn btn-info btn-xs" onclick="mostrar(' . $reg->idascensor . ')"><i class="fa fa-list-alt"></i></button><button class="btn btn-info btn-xs" onclick="editar(' . $reg->idascensor . ')"><i class="fa fa-pencil"></i></button>',
                "1" => $reg->codigo,
                "2" => $reg->edificio,
                "3" => $reg->contrato,
                "4" => $reg->valoruf,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
        
        
        case 'editar':
        $iduser = $_SESSION['iduser'];
        if (!empty($idascensor)) {
            $rspta = $ascensor->editar($idascensor, $idtascensor, $marca, $modelo, $ken, $pservicio, $gtecnica, $valoruf, $valorclp, $paradas, $capkg, $capper, $velocidad, $dcs, $elink, $iduser);
            echo $rspta ? "Ascensor editado" : "Ascensor no pudo ser editado";
        }
        break;
        
        case 'mostrar':
        $dascensor = $ascensor->mostrar($idascensor);

        if ($dascensor['dcs'] = 1) {
            $dascensor['dcs'] = "SI";
        } else {
            $dascensor['dcs'] = "NO";
        }

        if ($dascensor['elink'] = 1) {
            $dascensor['elink'] = "SI";
        } else {
            $dascensor['elink'] = "NO";
        }

        if (is_null($dascensor['codigo'])) {
            $dascensor['codigo'] = "S/C";
        }
        
        if (is_null($dascensor['ken'])) {
            $dascensor['ken'] = "S/C";
        }
        
        if (is_null($dascensor['valorclp'])) {
            $dascensor['valorclp'] = "-";
        }
        
        if (is_null($dascensor['paradas'])) {
            $dascensor['paradas'] = "-";
        }
        
        if (is_null($dascensor['capper'])) {
            $dascensor['capper'] = "-";
        }
        
        if (is_null($dascensor['capkg'])) {
            $dascensor['capkg'] = "-";
        }
        
        if (is_null($dascensor['velocidad'])) {
            $dascensor['velocidad'] = "-";
        }
        
        if (is_null($dascensor['gtecnica'])) {
            $dascensor['gtecnica'] = "S/F";
        }
        
        if (is_null($dascensor['pservicio'])) {
            $dascensor['pservicio'] = "S/F";
        }

        echo json_encode($dascensor);
        break;
        
        case 'formeditar':
        $rspta = $ascensor->formeditar($idascensor);
        echo json_encode($rspta);
        break;
    
        case 'dguia':
        $rspta = $ascensor->guia($codigo);
        echo json_encode($rspta);
        break;
    
        case 'selectasc':
			$rspta = $ascensor->SelectAsc();
                        echo '<option value="" selected disabled>SELECCIONE EQUIPO</option>';
			while($reg = $rspta->fetch_object()){
				echo '<option value='.$reg->codigo.' paradas='.$reg->paradas.' tipoequipo='.$reg->tipoequipo.'>'.$reg->codigo.' - '.$reg->nombre.' - '.$reg->tipoequipo.'</option>';
			}
	break;
        
        
}

 ?>