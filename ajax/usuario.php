<?php 
	session_start();
	require_once "../modelos/Usuario.php";
	require_once "../modelos/Role.php";


	$user = new Usuario();

	$iduser=isset($_POST["iduser"])?limpiarCadena($_POST["iduser"]):"";
	$idrole=isset($_POST["idrole"])?limpiarCadena($_POST["idrole"]):"";
	$username=isset($_POST["username"])?limpiarCadena($_POST["username"]):"";
	$password=isset($_POST["password"])?limpiarCadena($_POST["password"]):"";
	$nombre=isset($_POST["nombre"])?limpiarCadena($_POST["nombre"]):"";
	$apellido=isset($_POST["apellido"])?limpiarCadena($_POST["apellido"]):"";
	$tipo_documento=isset($_POST["tipo_documento"])?limpiarCadena($_POST["tipo_documento"]):"";
	$num_documento=isset($_POST["num_documento"])?limpiarCadena($_POST["num_documento"]):"";
	$fecha_nac=isset($_POST["fecha_nac"])?limpiarCadena($_POST["fecha_nac"]):"";
	$direccion=isset($_POST["direccion"])?limpiarCadena($_POST["direccion"]):"";
	$telefono=isset($_POST["telefono"])?limpiarCadena($_POST["telefono"]):"";
	$email=isset($_POST["email"])?limpiarCadena($_POST["email"]):"";


	switch ($_GET["op"]) {
		case 'guardaryeditar':
			if(!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name'])){
				$imagen=$_POST["imagenactual"];	
			}else{
				$ext = explode(".",$_FILES['imagen']['name']);
				if($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png" ){
					$imagen = round(microtime(true)).".".end($ext);
					move_uploaded_file($_FILES['imagen']['tmp_name'], "../files/usuarios/".$imagen);
				}
			}

			$clavehash = hash("SHA256", $password);

			if(empty($iduser)){
				$rspta=$user->insertar($idrole,$username,$clavehash,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$telefono,$email,$imagen);
				echo $rspta ? "Usuario registrado" : $username."Usuario no pudo ser registrado";
			}else{
				$rspta=$user->editar($iduser,$idrole,$username,$clavehash,$nombre,$apellido,$tipo_documento,$num_documento,$fecha_nac,$direccion,$telefono,$email,$imagen);
				echo $rspta ? "Usuario editado" : "Usuario no pudo ser editado";
			}
		break;

		case 'desactivar':
			$rspta=$user->desactivar($iduser);
			echo $rspta ? "Usuario inhabilitado" : "Usuario no se pudo inhabilitar";
		break;

		case 'activar':
			$rspta=$user->activar($iduser);
			echo $rspta ? "Usuario habilitado" : "Usuario no se pudo habilitar";
		break;

		case 'mostar':
			$rspta=$user->mostrar($iduser);
			echo json_encode($rspta);
		break;

		case 'listar':
			$rspta=$user->listar();
			$data = Array();
			while ($reg = $rspta->fetch_object()){
				$data[] = array(
					"0"=>($reg->condicion)?
					'<button class="btn btn-warning btn-xs" onclick="mostar('.$reg->iduser.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-danger btn-xs" onclick="desactivar('.$reg->iduser.')"><i class="fa fa-close"></i></button>':
					'<button class="btn btn-warning btn-xs" onclick="mostar('.$reg->iduser.')"><i class="fa fa-pencil"></i></button>'.
					' <button class="btn btn-primary btn-xs" onclick="activar('.$reg->iduser.')"><i class="fa fa-check"></i></button>',
					"1"=>$reg->nombre.''.$reg->apellido,
					"2"=>$reg->username,
					"3"=>$reg->email,
					"4"=>$reg->role,
					"5"=>($reg->condicion)?'<span class="label bg-green">Habilitado</span>':'<span class="label bg-red">Inhabilitado</span>'
				);
			}
			$results = array(
				"sEcho"=>1,
				"iTotalRecords"=>count($data),
				"iTotalDisplayRecords"=>count($data), 
				"aaData"=>$data
			);

			echo json_encode($results);
		break;

		case 'selectRole':
			require_once "../modelos/Role.php";
			$role = new Role();
			$rspta = $role->select();
			while($reg = $rspta->fetch_object()){
				echo '<option value='.$reg->idrole.'>'.$reg->nombre.'</option>';
			}
		break;

		case 'verificar':

			$username_form = $_POST['username_form'];
			$password_form = $_POST['password_form'];

			$password_hash = hash("SHA256", $password_form);

			$rspta=$user->verificar($username_form, $password_hash);

			$fecth = $rspta->fetch_object();

			if(isset($fecth)){
				$_SESSION['iduser']=$fecth->iduser;
				$_SESSION['nombre']=$fecth->nombre;
				$_SESSION['apellido']=$fecth->apellido;
				$_SESSION['imagen']=$fecth->imagen;
				$_SESSION['username']=$fecth->username;
				$_SESSION['idrole']=$fecth->idrole;
				$_SESSION['email']=$fecth->email;

				$role= new Role();
				$permisos = $role->listarmarcados($fecth->idrole);

				$valores=array();

				while ($per = $permisos->fetch_object()){
					array_push($valores, $per->idpermiso);
				}

				if(empty($fecth->firma) || empty($fecth->filefir)){
					$fecth->status = 'sinfirma';
				}else{
					$fecth->status = 'confirma';
				}
				
				in_array(1, $valores)? $_SESSION['administrador']=1:$_SESSION['administrador']=0;
				in_array(2, $valores)? $_SESSION['mantencion']=1:$_SESSION['mantencion']=0;
				in_array(3, $valores)? $_SESSION['Icontratos']=1:$_SESSION['Icontratos']=0;
				in_array(4, $valores)? $_SESSION['Mcontratos']=1:$_SESSION['Mcontratos']=0;
				in_array(5, $valores)? $_SESSION['Vcontratos']=1:$_SESSION['Vcontratos']=0;
				in_array(6, $valores)? $_SESSION['Lcontratos']=1:$_SESSION['Lcontratos']=0;
				in_array(7, $valores)? $_SESSION['Contratos']=1:$_SESSION['Contratos']=0;
				in_array(8, $valores)? $_SESSION['Guia']=1:$_SESSION['Guia']=0;
			}

			echo json_encode($fecth); 			
		break;

		case 'salir':
			session_unset();
			session_destroy();
			header("Location: ../index.php");
		break;

		case 'salirguia':
			session_unset();
			session_destroy();
			header("Location: ../production/guia/index.php");
		break;

		case 'getFirma':
			$rspta = $user->getFirma($_SESSION['iduser']);
			echo json_encode($rspta);
		break;

		case 'setFirma':
			$firma = isset($_POST["firma"]) ? limpiarCadena($_POST["firma"]) : '';
			if($firma){
				$encoded_image = explode(",", $firma)[1];
				$decoded_image = base64_decode($encoded_image);
				$imgfirma = round(microtime(true)) . ".png";
				$patchfir = "../files/usuarios/firmas/" . $imgfirma;
				file_put_contents($patchfir, $decoded_image);
				$rspta=$user->setFirma($_SESSION['iduser'], $firma, $imgfirma);
				echo $rspta;
			}else{
				echo 0;
			}
		break;
	}
?>