<?php
/*
UploadiFive
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
*/

// Set the uplaod directory
$uploadDir = '../../appfabrimetal/files/docsasociados/';

// Set the allowed file extensions
$fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // Allowed file extensions

if (!empty($_FILES)) {
	// $arrFiles = $_FILES;
	$totFiles = count($_FILES['file']['name']);
	for ($i = 0; $i<$totFiles; $i++)
	{
		$arrFiles[$i]['name'] = $_FILES['file']['name'][$i];
		$arrFiles[$i]['tmp_name'] = $_FILES['file']['tmp_name'][$i];
		$arrFiles[$i]['type'] = $_FILES['file']['type'][$i];
		$arrFiles[$i]['error'] = $_FILES['file']['error'][$i];
		$arrFiles[$i]['size'] = $_FILES['file']['size'][$i];
	}

	foreach ($arrFiles as $imgname) {
		if ($imgname)
		{
			$tempFile   = $imgname['tmp_name'];
			$targetFile = $uploadDir . $imgname['name'] . '___' . $_POST['dTime'] . '.TMP';

			// Validate the filetype
			$fileParts = pathinfo($imgname['name']);
			if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

				// Save the file
				move_uploaded_file($tempFile, $targetFile);
				echo 1;

			} else {

				// The file type wasn't allowed
				echo 'Invalid file type.';

			}
		}

	}
}
?>