<?php 

require "../config/conexion.php";

	Class Servicio{
		//Constructor para instancias
		public function __construct(){

		}

		public function iniciar($idtservicio, $iduser, $idtecnico, $idascensor, $estadoini, $observacionini, $latini, $lonini){
			$sql="INSERT INTO servicio (idtservicio, iduser, idtecnico, idascensor, estadoini, observacionini, latini, lonini) VALUES ('$idtservicio','$iduser','$idtecnico', '$idascensor', '$estadoini','$observacionini','$latini','$lonini')";
			return ejecutarConsulta($sql);
		}

		public function finalizar($idservicio,$estadofin, $observacionfin, $nrodocumento, $nombre, $apellido, $rut, $cargo, $firma, $latfin, $lonfin, $opfirma){
			if($opfirma == 1){
				$signature_time = ', signature_time=CURRENT_TIMESTAMP';
			}else{
				$signature_time = '';
			}
            if(is_null($nombre) && is_null($apellido) && is_null($rut) && is_null($cargo) && is_null($firma)){
                $sql="UPDATE servicio SET estadofin='$estadofin', observacionfin='$observacionfin', nrodocumento='$nrodocumento', closed_time=CURRENT_TIMESTAMP, latfin='$latfin', lonfin='$lonfin'".$signature_time." WHERE idservicio='$idservicio'";
            }else{
                $sql="UPDATE servicio SET estadofin='$estadofin', observacionfin='$observacionfin', nrodocumento='$nrodocumento', nombre='$nombre', apellidos='$apellido', rut='$rut', firma='$firma', closed_time=CURRENT_TIMESTAMP, latfin='$latfin', lonfin='$lonfin'".$signature_time."  WHERE idservicio='$idservicio'";
            }
			return ejecutarConsulta($sql);
		}
                
                public function firmar($idservicio, $nombre, $apellido, $rut, $cargo, $firma){
			$sql="UPDATE servicio SET nombre='$nombre', apellidos='$apellido', rut='$rut', firma='$firma', signature_time=CURRENT_TIMESTAMP WHERE idservicio='$idservicio'";
			return ejecutarConsulta($sql);
		}
                
                public function datosservicio($iduser){
		    $sql="SELECT s.idservicio, DATE(s.created_time) AS fecha, TIME(s.created_time) AS hora, s.estadoini, s.observacionini, w.nombre as tiposer, a.codigo, x.nombre AS tipo, m.nombre AS marca, n.nombre AS modelo, o.nombre as cargotec, t.nombre, t.apellidos, t.rut, e.nombre as edificio, e.calle, e.numero, a.idascensor, a.idtascensor as idtascensor FROM servicio s INNER JOIN ascensor a ON s.idascensor=a.idascensor INNER JOIN tascensor x ON a.idtascensor=x.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo=n.idmodelo INNER JOIN tecnico t ON s.idtecnico=t.idtecnico INNER JOIN tservicio w ON s.idtservicio=w.idtservicio INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN cargotec o ON t.idcargotec=o.idcargotec WHERE s.iduser='$iduser' AND s.estadofin IS NULL";
		    return ejecutarConsulta($sql);
		}
                
                public function verificarservicio($iduser){
		    $sql="SELECT idservicio FROM servicio WHERE iduser='$iduser' AND estadofin IS NULL";
		    return NumeroFilas($sql);
		}
                
                public function nofirma($idservicio){
		    $sql="UPDATE servicio SET reqfirma=0 WHERE idservicio='$idservicio'";
		    return ejecutarConsulta($sql);
		}
                
                public function infoguia($idservicio){
		    $sql="SELECT s.idservicio, DATE(s.created_time) AS fecha, TIME(s.created_time) AS hora, z.nombre AS estado , s.observacionini, w.nombre as tiposer, a.codigo, x.nombre AS tipo, m.nombre AS marca, n.nombre AS modelo, o.nombre as cargotec, t.nombre, t.apellidos, t.rut, e.nombre as edificio, e.calle, e.numero, a.idascensor, s.idtservicio, (SELECT infv_id FROM informevisita WHERE infv_servicio = '$idservicio' ORDER BY infv_id DESC LIMIT 0,1) as infvid, a.codigocli FROM servicio s INNER JOIN ascensor a ON s.idascensor=a.idascensor INNER JOIN tascensor x ON a.idtascensor=x.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo=n.idmodelo INNER JOIN tecnico t ON s.idtecnico=t.idtecnico INNER JOIN tservicio w ON s.idtservicio=w.idtservicio INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN cargotec o ON t.idcargotec=o.idcargotec INNER JOIN testado z ON s.estadoini = z.id WHERE s.idservicio='$idservicio'";
		    return ejecutarConsultaSimpleFila($sql);
		}
                
                public function LSF($iduser){
		    $sql="SELECT s.idservicio, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, a.codigo, e.nombre as edificio FROM servicio s INNER JOIN ascensor a ON s.idascensor=a.idascensor INNER JOIN edificio e ON a.idedificio=e.idedificio WHERE s.iduser='$iduser' AND s.reqfirma = 1 AND s.firma IS NULL AND s.estadofin IS NOT NULL order by fecha desc";
		    // $sql="SELECT s.idservicio, DATE(s.created_time) AS fecha, TIME(s.created_time) AS inicio, TIME(s.closed_time) AS fin, a.codigo, e.nombre as edificio FROM servicio s INNER JOIN ascensor a ON s.idascensor=a.idascensor INNER JOIN edificio e ON a.idedificio=e.idedificio WHERE s.iduser='$iduser' AND s.reqfirma = 1 AND s.firma IS NULL AND s.estadofin IS NOT NULL AND MONTH(s.created_time)  IN (MONTH(NOW()), MONTH(NOW())-1)";
		    return ejecutarConsulta($sql);
		}
                
                public function formfirma($idservicio){
		    $sql="SELECT s.idservicio, DATE(s.created_time) AS fechaini, TIME(s.created_time) AS horaini, DATE(s.closed_time) AS fechafin, TIME(s.closed_time) AS horafin, z.nombre AS estadoini, s.observacionini, p.nombre AS estadofn, s.observacionfin, w.nombre as tiposer, a.codigo, x.nombre AS tipo, m.nombre AS marca, n.nombre AS modelo, o.nombre as cargotec, t.nombre, t.apellidos, t.rut, e.nombre as edificio, e.calle, e.numero FROM servicio s INNER JOIN ascensor a ON s.idascensor=a.idascensor INNER JOIN tascensor x ON a.idtascensor=x.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo=n.idmodelo INNER JOIN tecnico t ON s.idtecnico=t.idtecnico INNER JOIN tservicio w ON s.idtservicio=w.idtservicio INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN cargotec o ON t.idcargotec=o.idcargotec INNER JOIN testado z ON s.estadoini = z.id INNER JOIN testado p ON s.estadofin=p.id WHERE s.idservicio='$idservicio'";
		    return ejecutarConsultaSimpleFila($sql);
		}
                
                public function email($idservicio){
			$sql="SELECT s.idservicio, u.nombre AS esini, s.observacionini, i.nombre AS esfin, s.observacionfin, s.created_time AS ini, s.closed_time AS fin, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascen, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tser, e.nombre AS edi, e.calle, e.numero, r.region_nombre AS region, c.comuna_nombre AS comuna, w.nombre AS segmen, p.nombre AS nomtec, p.apellidos AS apetec, p.rut AS ruttec, o.nombre AS cartec, s.file, s.filefir, s.nombre AS nomvali, s.apellidos AS apevali, s.rut AS rutvali, s.firma, s.reqfirma FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tsegmento w ON e.idtsegmento = w.idtsegmento INNER JOIN regiones r ON e.idregiones = r.region_id INNER JOIN comunas c ON e.idcomunas = c.comuna_id INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado u ON s.estadoini = u.id INNER JOIN testado i ON s.estadofin = i.id INNER JOIN tecnico p ON s.idtecnico = p.idtecnico INNER JOIN cargotec o ON p.idcargotec = o.idcargotec WHERE s.idservicio = '$idservicio'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function pdf($idservicio){
			$sql="SELECT s.idservicio, u.nombre AS esini, s.observacionini, i.nombre AS esfin, s.observacionfin, s.nombre AS nomvali, s.apellidos AS apevali, s.rut AS rutvali, s.firma, s.filefir, s.created_time AS ini, s.reqfirma, a.codigo, a.codigocli, a.ubicacion, q.nombre AS tascen, m.nombre AS marca, n.nombre AS modelo, t.nombre AS tser, e.nombre AS edi, e.calle, e.numero, r.region_nombre AS region, c.comuna_nombre AS comuna, p.nombre AS nomtec, p.apellidos AS apetec, p.rut AS ruttec FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN edificio e ON a.idedificio = e.idedificio INNER JOIN tascensor q ON a.idtascensor = q.idtascensor INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo = n.idmodelo INNER JOIN tsegmento w ON e.idtsegmento = w.idtsegmento INNER JOIN regiones r ON e.idregiones = r.region_id INNER JOIN comunas c ON e.idcomunas = c.comuna_id INNER JOIN tservicio t ON s.idtservicio = t.idtservicio INNER JOIN testado u ON s.estadoini = u.id INNER JOIN testado i ON s.estadofin = i.id INNER JOIN tecnico p ON s.idtecnico = p.idtecnico INNER JOIN cargotec o ON p.idcargotec = o.idcargotec WHERE s.idservicio = '$idservicio'";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function UpFile($archivo, $idservicio){
                    $sql="UPDATE servicio SET file='$archivo'WHERE idservicio='$idservicio'";
			return ejecutarConsulta($sql);
                }
                
                public function UpFirma($archivo, $idservicio){
                    $sql="UPDATE servicio SET filefir='$archivo'WHERE idservicio='$idservicio'";
			return ejecutarConsulta($sql);
                }
                
                public function SolPre($idservicio, $idascensor, $idsupervisor, $idtecnico, $descripcion){
                    $sql="INSERT INTO presupuesto (idservicio, idascensor, idsupervisor, idtecnico, descripcion) VALUES ('$idservicio','$idascensor','$idsupervisor','$idtecnico','$descripcion')";
			return ejecutarConsulta($sql);
                }

                public function SolPreWithImg($idservicio, $idascensor, $idsupervisor, $idtecnico, $descripcion, $imgs, $dTime){
                	$i = 0;
                    $sql="INSERT INTO presupuesto (idservicio, idascensor, idsupervisor, idtecnico, descripcion) VALUES ('$idservicio','$idascensor','$idsupervisor','$idtecnico','$descripcion')";
					$result = ejecutarConsu_retornarID($sql);
					if ($result)
					{
						//$imgs es array
						foreach ($imgs as $imgname) {
							if ($imgname)
							{
								$i++;
								$tmp = explode(".", $imgname);
								$extension = end($tmp);
								$imgname = $imgname . '___' . $dTime . '.TMP';
								$newImgName = 'presupuesto-' . $result . '-' . $i . '-' . date('YmdHis') . '.' . $extension;
								rename("../../appfabrimetal/files/docsasociados/$imgname", "../../appfabrimetal/files/docsasociados/$newImgName");
								
								/*
								$path_parts = pathinfo('/www/htdocs/index.html');
								echo $path_parts['dirname'], "\n";
								echo $path_parts['basename'], "\n";
								echo $path_parts['extension'], "\n";
								echo $path_parts['filename'], "\n"; // since PHP 5.2.0*/

								/*$filename=$_FILES["picture"]["tmp_name"];
								$extension=end(explode(".", $filename));
								$newfilename="$_POST[lastname]" . '&#95;' . "$_POST[firstname]".".".$extension;
								move_uploaded_file($filename, "peopleimages/" .$newfilename);*/

								$sql="INSERT INTO docsasociados (proceso, tabla, datoid, documento) VALUES ('GSERVICIO_PRESUPUESTO', 'presupuesto','$result','$newImgName')";

								ejecutarConsulta($sql);
							}
						}

					}
					return 1;
                }
                
                public function camfirma($idservicio){
			$sql="SELECT firma FROM servicio WHERE idservicio = '$idservicio' ";
			return ejecutarConsultaSimpleFila($sql);
		}
                
                public function Idsts($idservicio){
			$sql="SELECT ser.idtecnico, tec.idsupervisor FROM servicio ser INNER JOIN tecnico tec ON ser.idtecnico = tec.idtecnico WHERE ser.idservicio = '$idservicio'";
			return ejecutarConsultaSimpleFila($sql);
		}

		//funcion que resuelve si se puede terminar(cerrar) el servicio de la guia
		//tomando como referencia la hora de inicio del servicio, paradas del equipo
		//y la fecha y hora actual
		//solo valido para equipos ASCENSOR y tipo MANTENCION - 20220310
		//retorna campos cerrarguia y fechamincierre
		public function guiaPorCerrar($idservicio) { //0 = no se puede cerrar, 1 = se puede cerrar
			$minutosXparada = 5;
			$sql = "SELECT (CASE WHEN (RES.tipoequipo='ASCENSOR' AND RES.tiposervicio='MANTENCION') THEN CASE WHEN RES.fechaactual>=date_add(RES.fechacreacion,INTERVAL RES.minutosespera MINUTE) THEN 1 ELSE 0 END ELSE 1 END) AS cerrarguia,date_add(RES.fechacreacion,INTERVAL RES.minutosespera MINUTE) AS fechamincierre FROM (SELECT A.paradas*{$minutosXparada} AS minutosespera,TA.nombre AS tipoequipo,TS.nombre AS tiposervicio,S.created_time AS fechacreacion,CURRENT_TIMESTAMP () AS fechaactual FROM ascensor AS A INNER JOIN servicio AS S ON A.idascensor=S.idascensor INNER JOIN tascensor AS TA ON A.idtascensor=TA.idtascensor INNER JOIN tservicio AS TS ON S.idtservicio=TS.idtservicio WHERE S.idservicio={$idservicio}) AS RES";
			return ejecutarConsultaSimpleFila($sql);
		}
                
	}
?>