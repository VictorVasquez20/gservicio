<?php 

	require "../config/conexion.php";

	Class Ascensor{
		//Constructor para instancias
		public function __construct(){

		}

		public function InsertarIds($idascensor, $codigo, $iduser){
			$sql="UPDATE ascensor SET codigo= '$codigo', updated_time=CURRENT_TIMESTAMP, updated_user='$iduser' WHERE idascensor='$idascensor'";
			return ejecutarConsulta($sql);
		}

		public function insertar($iduser,$idedicon,$idtascensor,$marca,$modelo, $valoruf, $valorclp, $paradas, $capper, $capkg, $velocidad, $pservicio, $gtecnica, $ken, $dcs, $elink){
			$sql="INSERT INTO ascensor (iduser, idedificio_contrato, idtascensor, marca, modelo, ken, paradas, capper, capkg, velocidad, dcs, elink, valoruf, valorclp, pservicio, gtecnica, condicion,  created_user) VALUES ('$iduser', '$idedicon', '$idtascensor','$marca','$modelo', '$ken', '$paradas', '$capper', '$capkg', '$velocidad', '$dcs', '$elink', '$valoruf', '$valorclp', '$pservicio', '$gtecnica', 1,'$iduser')";
			return ejecutarConsulta($sql);
		}


		public function solid_ascid($idcontrato){
			$sql="SELECT a.idascensor, m.nombre as marca, o.nombre as modelo, e.nombre, e.calle, e.numero, e.idedificio, r.region_nombre, r.region_ordinal FROM ascensor a INNER JOIN edificio_contrato w ON a.idedificio_contrato=w.idedificio_contrato INNER JOIN edificio e ON w.idedificio=e.idedificio INNER JOIN regiones r ON e.idregiones = r.region_id INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo o ON a.modelo=o.idmodelo WHERE w.idcontrato='$idcontrato' AND a.codigo IS null";
			return ejecutarConsulta($sql);
		}

		public function solid_edifid($idcontrato){
			$sql="SELECT COUNT(a.idascensor) AS nascensores, e.idedificio FROM ascensor a INNER JOIN edificio_contrato w ON a.idedificio_contrato=w.idedificio_contrato INNER JOIN edificio e ON w.idedificio=e.idedificio WHERE w.idcontrato='$idcontrato' GROUP BY e.idedificio";
			return ejecutarConsulta($sql);
		}

		public function contar_ascensores($idcontrato){
			$sql="SELECT COUNT(idascensor) AS nascensores FROM ascensor a INNER JOIN edificio_contrato q ON a.idedificio_contrato=q.idedificio_contrato WHERE q.idcontrato='$idcontrato'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function ascensores_contrato($idcontrato){
			$sql="SELECT a.codigo, m.nombre AS marca, b.nombre AS modelo, a.valoruf, e.nombre AS edificio, r.region_nombre AS region, n.comuna_nombre AS comuna FROM ascensor a INNER JOIN edificio_contrato q ON a.idedificio_contrato=q.idedificio_contrato INNER JOIN edificio e ON q.idedificio = e.idedificio INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo b ON a.modelo = b.idmodelo INNER JOIN regiones r ON e.idregiones = r.region_id INNER JOIN comunas n ON e.idcomunas = n.comuna_id  WHERE q.idcontrato = '$idcontrato'";
			return ejecutarConsulta($sql);
		}

		public function ascensores_edificio($idedificio){
			$sql="SELECT a.codigo, m.nombre AS marca, b.nombre AS modelo, a.valoruf,c.ncontrato, x.nombre AS tipo FROM ascensor a INNER JOIN edificio_contrato q ON a.idedificio_contrato=q.idedificio_contrato INNER JOIN contrato c ON q.idcontrato=c.idcontrato INNER JOIN edificio e ON q.idedificio = e.idedificio INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo b ON a.modelo = b.idmodelo INNER JOIN tascensor x ON a.idtascensor=x.idtascensor WHERE e.idedificio='$idedificio'";
			return ejecutarConsulta($sql);
		}

		public function ascensores_cliente($idcliente){
			$sql="SELECT a.codigo, m.nombre AS marca, b.nombre AS modelo, a.valoruf,c.ncontrato, e.nombre AS edificio, x.nombre AS tipo FROM ascensor a INNER JOIN edificio_contrato q ON a.idedificio_contrato=q.idedificio_contrato INNER JOIN contrato c ON q.idcontrato=c.idcontrato INNER JOIN contrato_cliente r ON c.idcontrato = r.idcontrato INNER JOIN edificio e ON q.idedificio = e.idedificio INNER JOIN marca m ON a.marca = m.idmarca INNER JOIN modelo b ON a.modelo = b.idmodelo INNER JOIN tascensor x ON a.idtascensor=x.idtascensor WHERE r.idcliente = '$idcliente'";
			return ejecutarConsulta($sql);
		}

		public function listar(){
			$sql="SELECT a.idascensor, a.codigo, a.valoruf, e.nombre AS edificio, c.ncontrato AS contrato  FROM ascensor a INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN contrato c ON a.idcontrato = c.idcontrato ORDER BY e.nombre ASC";
			return ejecutarConsulta($sql);
		}

		public function editar($idascensor, $idtascensor, $marca, $modelo, $ken, $pservicio, $gtecnica, $valoruf, $valorclp, $paradas, $capkg, $capper, $velocidad, $dcs, $elink, $iduser){
			$sql="UPDATE ascensor SET idtascensor='$idtascensor', marca='$marca', modelo='$modelo', ken='$ken', pservicio='$pservicio', gtecnica='$gtecnica', valoruf='$valoruf', valorclp='$valorclp', paradas='$paradas', capkg='$capkg', capper='$capper', velocidad='$velocidad', dcs='$dcs', elink='$elink', updated_time=CURRENT_TIMESTAMP, updated_user='$iduser' WHERE idascensor='$idascensor'";
			return ejecutarConsulta($sql);
		}

		public function mostrar($idascensor){
			$sql="SELECT a.*, e.nombre AS edificio, c.ncontrato AS contrato, i.razon_social AS cliente, n.nombre AS modelo, m.nombre AS marca, v.nombre AS tipo FROM ascensor a INNER JOIN edificio_contrato	w ON a.idedificio_contrato = w.idedificio_contrato INNER JOIN edificio e ON w.idedificio=e.idedificio INNER JOIN contrato c ON w.idcontrato = c.idcontrato INNER JOIN contrato_cliente q ON c.idcontrato=q.idcontrato INNER JOIN cliente i ON q.idcliente = i.idcliente INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo=n.idmodelo INNER JOIN tascensor v ON a.idtascensor = v.idtascensor WHERE a.idascensor='$idascensor'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function formeditar($idascensor){
			$sql="SELECT * FROM ascensor WHERE idascensor='$idascensor'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function guia($codigo){
			$sql="SELECT a.idascensor, a.codigo , e.nombre AS edificio, e.calle, e.numero, n.nombre AS modelo, m.nombre AS marca, v.nombre AS tipo, w.nombre AS estado, w.id AS idtestado, DATE_FORMAT(a.gtecnica, '%d-%m-%Y') gtecnica, a.codigocli FROM ascensor a INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN marca m ON a.marca=m.idmarca INNER JOIN modelo n ON a.modelo=n.idmodelo INNER JOIN tascensor v ON a.idtascensor = v.idtascensor INNER JOIN testado w ON a.estado=w.id WHERE a.codigo='$codigo'";
			return ejecutarConsultaSimpleFila($sql);
		}

		public function UpEstado($idascensor, $estado, $codigocli){
			$sql="UPDATE ascensor SET estado='$estado', codigocli = '$codigocli' WHERE idascensor='$idascensor'";
			return ejecutarConsulta($sql);
		}

		public function UpEstado2($idascensor, $estado){
			$sql="UPDATE ascensor SET estado='$estado' WHERE idascensor='$idascensor'";
			return ejecutarConsulta($sql);
		}

		public function SelectAsc(){
			$sql="SELECT a.codigo, e.nombre, t.nombre as tipoequipo, a.paradas FROM ascensor a INNER JOIN edificio e ON a.idedificio=e.idedificio INNER JOIN tascensor t ON a.idtascensor = t.idtascensor WHERE a.codigo IS NOT NULL";
			return ejecutarConsulta($sql);
		}

		public function EmailSup($idservicio){
			$sql="SELECT sup.email_interno AS email FROM servicio s INNER JOIN ascensor a ON s.idascensor = a.idascensor INNER JOIN supervisor sup ON a.idsup = sup.idsupervisor WHERE s.idservicio = '$idservicio'";
			return ejecutarConsultaSimpleFila($sql);
		}
	}
?>