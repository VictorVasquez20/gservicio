<?php 
ob_start();
session_start();

if(!isset($_SESSION["nombre"])){
  header("Location:login.php");
}else{

require 'header.php';

/*if( $_SESSION['administrador']==1)
{*/

 ?>
        
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Firma Digital</h2>
                    <div class="clearfix"></div>
                  </div>

                  <!-- formulario para captar la firma electronica del empleado
                  y poder firmar el acta de entrega. firma se guardara en una tabla -->
                  <div id="formulariofirdig">
                    <br />
                    <div class="">
                      <form class="form-horizontal form-label-left" id="formulariofirmadigital" name="formulariofirmadigital">
                        <input type="hidden" name="firma" id="firma">
                        <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <h4><b>Actualización de mi firma digital</b></h4>
                        </div>

                        <div id="formfirma" name="formfirma">
                          <div class="col-md-12 col-sm-12 col-xs-12 form-group" required="Campo requerido">
                            <label>Firma</label>
                            <div class="input-group">
                            <input type="text" disabled="disabled" class="form-control" name="firmavali" id="firmavali" required="Campo requerido">
                            <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opciones <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li><a onclick="resetearfirma()">Firma cargada</a></li>
                            <li class="divider"></li>
                            <li><a onclick="borrarfirma()">Limpiar</a></li></ul>
                            </div>
                            </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 form-group" id="firmapad" name="firmapad" >
                            <canvas id="firmafi" id="firmafi" class="firmafi" style="border: 2px dashed #888; width: 100%;"></canvas>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <button class="btn btn-primary" type="button" id="btnCancelar" onclick="cancelarform()">Cancelar</button>
                            <button class="btn btn-primary" type="reset" id="btnLimpiar" onclick="limpiar()">Limpiar</button>
                            <button class="btn btn-success" type="submit" id="btnGuardar">Actualizar</button>
                            <button class="btn btn-danger" type="button" id="btnFirmar" onclick="firmarform()">Firmar</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

<?php 
/*}else{
  require 'nopermiso.php';
}*/
require 'footer.php';
?>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<!-- Script no refresca a veces los cambios en los dispositivos moviles. Se cambia la forma de cargar por uno random -->
<script id="myScript"></script>
<script>
  var url = 'scripts/digitalsignature.js';
  var extra = '?t=';
  var randomNum = String((Math.floor(Math.random() * 20000000000)));
  document.getElementById('myScript').src = url + extra + randomNum;
</script>
<?php 
}
ob_end_flush();
?>