
$("#frmAcceso").on('submit', function(e){
	e.preventDefault();
	username = $("#username").val();
	password = $("#password").val();
	$.post("../ajax/usuario.php?op=verificar",{"username_form": username, "password_form": password}, function(data){
		if(data!="null"){
			var obj = $.parseJSON(data);
			if(obj.status === 'sinfirma'){
				$(location).attr("href", "digitalsignature.php");
			}else{
				$(location).attr("href", "estado.php");
			}
			return false;
		}else{
			bootbox.alert("Usuario o Password Incorrectos")
		}

	})

})

$("#frmAccesoGuia").on('submit', function(e){
	e.preventDefault();
	username = $("#username").val();
	password = $("#password").val();
	$.post("../../ajax/usuario.php?op=verificar",{"username_form": username, "password_form": password}, function(data){
		if(data!="null"){
			$(location).attr("href", "guia.php");
		}else{
			bootbox.alert("Usuario o Password Incorrectos")
		}

	})

})