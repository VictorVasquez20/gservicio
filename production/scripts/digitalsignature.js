var tabla;
var firmaSave;
var canvas;

//funcion que se ejecuta iniciando
function init(){
	$("#btnFirmar").hide();

	$('[data-toggle="tooltip"]').tooltip(); 

	$("#formulariofirmadigital").on("submit", function(e){
		e.preventDefault();
		setFirma();
	})


	canvas = document.getElementById('firmafi');
	canvas.height = canvas.offsetHeight;
	canvas.width = canvas.offsetWidth;
	signaturePad = new SignaturePad(canvas, {
		backgroundColor: 'rgb(255, 255, 255)',
		penColor: 'rgb(0, 0, 0)'
	});

	var el = document.getElementById("t");
	canvas.addEventListener("click", firmamodificada, false);
	canvas.addEventListener("touchstart", firmamodificada, false);
	canvas.addEventListener("touchend", firmamodificada, false);

	firmaSave = '';
	$.post("../ajax/usuario.php?op=getFirma", function(data){
		data = JSON.parse(data);
		firmaSave = $.trim(data.firma) + '';
		if (firmaSave != null && firmaSave != '') {
			signaturePad.fromDataURL(firmaSave, { width: canvas.offsetWidth, height: canvas.offsetHeight});
			$("#firma").val(firmaSave);
			$("#firmavali").val("Firma cargada");
			$("#firmavali").addClass(' border border-success');
		}
	});
}

function firmamodificada(){
	fijarfirma();
}

function cancelarform(){
}

function firmarform(){
	var formData = new FormData($("#formulariofirmadigital")[0]);
	$.ajax({
		url:'../ajax/asignacion.php?op=firmar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
		}
	});
}


function fijarfirma(){
	if(!signaturePad.isEmpty()){
		const padfirma = signaturePad.toDataURL();
		if(padfirma){
			console.log('firma fijada');
			$("#firmavali").val("Firma modificada");
			// $("#firmavali").val("Firma validada");
			$("#firmavali").addClass(' border border-success');
			$("#firma").val(padfirma);
		}else{
			$("#firmavali").val("Error al validar");
			$("#firmavali").addClass(' border border-danger');
		}     
	}
}


function borrarfirma(){
	signaturePad.clear();
	$("#firma").val('');
	$("#firmapad").show();
	$("#firmavali").val('');
	$("#btnFirmar").hide();
}

function resetearfirma(){
	fijarfirma();
	signaturePad.clear();
	signaturePad.fromDataURL(firmaSave, { width: canvas.offsetWidth, height: canvas.offsetHeight});
	$("#firma").val(firmaSave);
	$("#firmapad").show();
	$("#firmavali").val('Firma cargada');
	$("#btnFirmar").hide();
}

function setFirma(){
	var formData = new FormData($("#formulariofirmadigital")[0]);
	$.ajax({
		url:'../ajax/usuario.php?op=setFirma',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			var mensaje = (datos == 1) ? '<span style="color:green; font-weight: bold">Firma actualizada correctamente</span>' : '<span style="color:red">Firma esta vacía. Favor registre su firma.</span>';
			if(mensaje){
				bootbox.alert(mensaje);
			}
		}
	});
}

init();