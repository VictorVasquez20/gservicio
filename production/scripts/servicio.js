var tabla;
var signaturePad;
var periodo;
var msg;
var json;

//funcion que se ejecuta iniciando
function init(){
	mostarform();
	$.post("../ajax/servicio.php?op=Verificar", function(r){
		$("#inicio").html(r);
		$.post("../ajax/ascensor.php?op=selectasc", function(r){
			$("#codigo").html(r);
			$("#codigo").selectpicker('refresh');
		});
	});

	$('#formularioinicio').on("submit", function(event){
		event.preventDefault();

        let mensaje = mensajeParadas();

        bootbox.confirm(mensaje + "¿Confirma que desea iniciar el servicio?", function (result) {
            if (result) {
        		if(navigator.geolocation){
        			navigator.geolocation.getCurrentPosition(
        				function(position){
        					$("#latitudso").val(position.coords.latitude);
        					$("#longitudso").val(position.coords.longitude);
        					IniciarServ();
        				},function(error){
        					new PNotify({
        						title: 'Geolocalización',
        						text: showError(error.code),
        						type: 'warning',
        						styling: 'bootstrap3'
        					});
                            $("#latitudso").val("-33.385232");
                            $("#longitudso").val("-70.776407");
                            IniciarServ();
        				},{timeout:5000}
        			);
        			//return false;
        		}else{
        			new PNotify({
        				title: 'Geolocalización',
        				text: "La Geolocalización no es soportada por este navegador.",
        				type: 'error',
        				styling: 'bootstrap3'
        			});
        			$("#latitudso").val("-33.385232");
                    $("#longitudso").val("-70.776407");
                    IniciarServ();
        		}
        		return false;
            }
        });

	});

	

	$('#formulariofin').on("submit", function(event){
		event.preventDefault();
		if(fijarfirma()){
			$("#btnGuardar").prop("disabled", true);
			var formData = new FormData($("#formulariofin")[0]);

			bootbox.confirm("¿Desea dar finalizado el servicio?", function (result) {
				if(result){
					FinalizarServ();
				}
			});
		}else{
			$("#btnGuardar").prop("disabled", true);
			var formData = new FormData($("#formulariofin")[0]);

			bootbox.confirm("¿Desea dar finalizado el servicio?", function (result) {
				if (result) {
					FinalizarServ();
				}
			});
		}
	});

	$('body').on('submit', '#formnewinforme', function(e) {
		guardarinforme(e);
	});
}

function showError(error) {
		switch(error.code) {
			case error.PERMISSION_DENIED:
				msg = "El usuario no permitió informar su posición.";
			break;
			case error.POSITION_UNAVAILABLE:
				msg = "El dispositivo no pudo recuperar la posición actual.";
			break;
			case error.TIMEOUT:
				msg = "Se ha superado el tiempo de espera.";
			break;
			case error.UNKNOWN_ERROR:
				msg = "Un error ha ocurrido.";
			break;
		}
		return msg;
	}

function mostarform(){
	$("#inicioservicio").hide();
	$("#finservicio").hide();
	$("#inicio").show();		
}

function BuscarEquipo(){

	// if (navigator.geolocation) {
	// 	console.log("Entro a obtener ubicacion");
	// 	navigator.geolocation.getCurrentPosition(fijarUbicacion);
	// }else{
	// 	console.log("No logro obtener la ubicacion");
	// 	$("#latitudso").val("-33.385232");
	// 	$("#longitudso").val("-70.776407");
	// }
	console.log($("#codigo").val());

	if($("#codigo").val() == null){
		new PNotify({
			title: 'Error en el equipo',
			text: 'Debe seleccionar un equipo',
			type: 'error',
			styling: 'bootstrap3'
		});
	}else{
		$.post("../ajax/ascensor.php?op=dguia",{codigo:$("#codigo").val()}, function(data,status){             
			$("#inicio").hide();
			$("#inicioservicio").show();
			data = JSON.parse(data);
			$("#idascensorso").val(data.idascensor);
			$("#codigoso").val(data.codigo);
			$("#tascensorso").val(data.tipo);	
			$("#marcaso").val(data.marca);  
			$("#modeloso").val(data.modelo);
			$("#edificioso").val(data.edificio);
			$("#direccionso").val(data.calle+' '+data.numero);
			$.post("../ajax/tservicio.php?op=selecttservicio", function(r){
				$("#idtservicioso").html(r);
				$("#idtservicioso").selectpicker('refresh');
			});
			// $("#latitudso").val("-33.385232");
			// $("#longitudso").val("-70.776407");
			$("#idtestadoso").val(data.idtestado);
			$("#estadoiniso").val(data.estado); 
			$("#garantia").val(data.gtecnica); 
			$("#codcli").val(data.codigocli);
		});
	}
}

// function fijarUbicacion(position){
// 	$("#latitudso").val(position.coords.latitude);
// 	$("#longitudso").val(position.coords.longitude);
// }

// function fijaPredeterminada(fail){
// 	console.log(fail.message);
// 	if(fail.message.indexOf("Only secure origins are allowed") == 0) {
// 		$("#latitudso").val("-33.385232");
// 		$("#longitudso").val("-70.776407");
// 	}
// }



function listar(){
	tabla=$('#tblsolicitudesid').dataTable({
		"aProcessing":true,
		"aServerSide": true,
		dom: 'Bfrtip',
		buttons:[
			'copyHtml5',
			'print',
			'excelHtml5',
			'csvHtml5',
			'pdf'
		],
		"ajax":{
			url:'../ajax/contrato.php?op=listarsoid',
			type:"get",
			dataType:"json",
			error: function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy": true,
		"iDisplayLength": 10, //Paginacion 10 items
		"order" : [[1 , "desc"]] //Ordenar en base a la columna 0 descendente
	}).DataTable();
}


function IniciarServ(){
	var formData = new FormData($("#formularioinicio")[0]);
	$("#btnIniciar").prop("disabled", true);
	$.ajax({
		url:'../ajax/servicio.php?op=iniciar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos, function(){ 
				$(location).attr("href", "estado.php");
			});
		}
	});
}

function FinalizarServ(){
	var formData = new FormData($("#formulariofin")[0]);
	$("#btnFinalizar").prop("disabled", true);
	$.ajax({
		url:'../ajax/servicio.php?op=finalizar',
		type:"POST",
		data:formData,
		contentType: false,
		processData:false,

		success: function(datos){
			bootbox.alert(datos, function(){ 
				$(location).attr("href", "estado.php");
			});
		}
	});
}

function formfinalizar(idservicio, idascensor){
    let data = '';
    $.post("../ajax/servicio.php?op=guiaPorCerrar",{idserviciofi:idservicio}, function(data,status){
        data = JSON.parse(data);
        // alert(data.cerrarguia);
        if (data.cerrarguia == 0) {
            bootbox.alert('Puede terminar este servicio despues de esta fecha y hora: ' + data.fechamincierre);
            // alert('Fecha mínima de termino de servicio: ' + data.fechamincierre);
            return;
        }
        else {
            // alert('SI SE PUEDE CERRAR');
            // return;
        	if(navigator.geolocation){
        		navigator.geolocation.getCurrentPosition(
        			function(position){
						$("#latitudfi").val(position.coords.latitude);
						$("#longitudfi").val(position.coords.longitude);
        			},function(error){
        				$("#latitudfi").val("-33.385232");
        				$("#longitudfi").val("-70.776407");
        				new PNotify({
        					title: 'Geolocalización',
        					text: showError(error.code),
        					type: 'warning',
        					styling: 'bootstrap3'
        				});
        			},{timeout:5000}
        		);
        		//return false;
        	}else{
        		new PNotify({
        			title: 'Geolocalización',
        			text: "La Geolocalización no es soportada por este navegador.",
        			type: 'warning',
        			styling: 'bootstrap3'
        		});
				$("#latitudfi").val("-33.385232");
				$("#longitudfi").val("-70.776407");
        	}
        	$.post("../ajax/servicio.php?op=infoguia",{idserviciofi:idservicio}, function(data,status){             
				$("#inicio").hide();
				$("#finservicio").show();
				data = JSON.parse(data);
				$("#codigofi").val(data.codigo);
				$("#tascensorfi").val(data.tipo);
				$("#marcafi").val(data.marca);
				$("#modelofi").val(data.modelo);
				$("#codclifi").val(data.codigocli);
				$("#edificiofi").val(data.edificio);
				$("#direccionfi").val(data.calle +' '+ data.numero);
				$("#fechainifi").val(data.fecha);
				$("#horainifi").val(data.hora);
				$("#tserviciofi").val(data.tiposer);
				$("#estadoinifi").val(data.estado);
				$("#observacioniniso").val(data.observacionini);
				if (data.idtservicio == 1 || data.idtservicio == 4) {
					$("#formdoc").empty();
						var html = '<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                          <label>Número presupuesto</label>'+
							'<input type="text" class="form-control" name="nrodocumento" id="nrodocumento" required="Campo requerido">'+
							'</div> ';
						$("#formdoc").append(html);
				}else if(data.idtservicio == 2){
					$("#formdoc").empty();
					var html = '<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                          <label>Número de ticket de emergencia</label>'+
						'<input type="text" class="form-control" name="nrodocumento" id="nrodocumento" required="Campo requerido">'+
						'</div> ';
					$("#formdoc").append(html);
					var html2 = '<div class="col-md-12 col-sm-12 col-xs-12 form-group"><label>¿Falla por terceros?</label>'+
						'<select class="form-control" name="terceros" id="terceros"><option value="false">NO</option><option value="true">SI</option></select>'+
						'</div> ';
					$("#formdoc").append(html2);
				}else if(data.idtservicio == 3){ //MANTENCION
					$("#formdoc").empty();
					if (data.infvid) //ultimo id del informe de mantenimiento para el servicio, campo queda readonly
						var html = '<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                          <label>Número de lista de inspección</label>'+
							'<input type="text" class="form-control" name="nrodocumento" id="nrodocumento" required="Campo requerido" readonly>'+
							'</div> ';
					else
						var html = '<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                          <label>Número de lista de inspección</label>'+
							'<input type="text" class="form-control" name="nrodocumento" id="nrodocumento" required="Campo requerido">'+
							'</div> ';
						$("#formdoc").append(html);
						$("#nrodocumento").val(data.infvid);
				}else if(data.idtservicio == 5){
					$("#formdoc").empty();
					var html = '<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                          <label>Numero de ticket ingenieria</label>'+
						'<input type="text" class="form-control" name="nrodocumento" id="nrodocumento">'+
						'</div> ';
					$("#formdoc").append(html);
				}
				$.post("../ajax/testado.php?op=selecttestadofi", function(r){
					$("#idestadofi").html(r);
					$("#idestadofi").selectpicker('refresh');
				});
				$("#idserviciofi").val(data.idservicio);
				$("#idascensorfi").val(data.idascensor);
			});
        	return false;
        }
    });

}

function addvalidform(){
	$("#formfirma").empty();
	var opcion= $("#opfirma").val();
	if (opcion == '1'){
		var myvar = '<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                          <label>Nombres</label>'+
			'<input type="text" class="form-control" name="nombresfi" id="nombresfi" required="Campo requerido">'+
			'</div> '+
			'<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                           <label>Apellidos</label>'+
			'<input type="text" class="form-control" name="apellidosfi" id="apellidosfi" required="Campo requerido">'+
			'</div> '+
			'<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                         <label>Rut</label>'+
			'<input type="text" class="form-control" name="rutfi" id="rutfi" required="Campo requerido">'+
			'</div> '+
			'<div class="col-md-12 col-sm-12 col-xs-12 form-group">                                           <label>Cargo</label>'+
			'<input type="text" class="form-control" name="cargofi" id="cargofi" required="Campo requerido">'+
			'</div>'+
			'<div class="col-md-12 col-sm-12 col-xs-12 form-group" required="Campo requerido">                                           <label>Firma</label>'+ 
			'<div class="input-group">'+
			'<input type="hidden" name="firma" id="firma">'+
			'<input type="text" disabled="disabled" class="form-control" name="firmavali" id="firmavali" required="Campo requerido">'+
			'<div class="input-group-btn">'+
			'<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Opciones <span class="caret"></span></button>'+
			'<ul class="dropdown-menu dropdown-menu-right" role="menu">'+
			'<li><a onclick="fijarfirma()">Validar</a></li>'+
			'<li class="divider"></li>'+
			'<li><a onclick="borrarfirma()">Borrar firma</a></li></ul>'+
			'</div>'+
			'</div>'+
			'</div>'+
			'<div class="col-md-12 col-sm-12 col-xs-12 form-group" id="firmapad" name="firmapad" >'+
			'<div class="well">'+
			'<canvas id="firmafi" id="firmafi" class="firmafi" style="border: 2px dashed #888; width: 100%;"></canvas>'+
			'</div>'+
			'</div>';
		$("#formfirma").append(myvar);

		var canvas = document.getElementById('firmafi');
		canvas.height = canvas.offsetHeight;
		canvas.width = canvas.offsetWidth;
		signaturePad = new SignaturePad(canvas, {
			backgroundColor: 'rgb(255, 255, 255)',
			penColor: 'rgb(0, 0, 0)'
		});
	}
}

function addpreform(){
	$("#formpre").empty();
	var opcion= $("#oppre").val();
	if (opcion == '1'){
		$("#zonaUpload").show();
		var myvar = '<div class="col-md-12 col-sm-12 col-xs-12 form-group"><label for="observacionfi">Descripcion de la solicitud</label>'+
			'<textarea type="text" id="descripcion" name="descripcion" required="Campo requerido" class="resizable_textarea form-control"></textarea>'+
			'</div> ';
		$("#formpre").append(myvar);
	}else{
		$("#zonaUpload").hide();
	}
}

function fijarfirma(){
	if ($('#porfirmar').val() == 'true') { //campo nuevo para saber si se posterga o no la firma, default true
		if ($('#firmapad').length) {
			if(signaturePad.isEmpty()){
				new PNotify({
					title: 'Error en la firma',
					text: 'La firma no puede estar vacia',
					type: 'error',
					styling: 'bootstrap3'
				});
			}else{
				const padfirma = signaturePad.toDataURL();
				if(padfirma){
					$("#firmavali").val("Firma validada");
					$("#firmavali").addClass(' border border-success');
					$("#firma").val(padfirma);
					$("#firmapad").hide();
					return 1;
				}else{
					$("#firmavali").val("Error al validar");
					$("#firmavali").addClass(' border border-danger');
				}     
			}
		}
	}else{
		return 1;
	}
}


function borrarfirma(){
	signaturePad.clear();
	$("#firmapad").show();
}

function ScanQR(){
	let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
	scanner.addListener('scan', function (content) {
		$("#codigo").val(content);
	});

	Instascan.Camera.getCameras().then(function (cameras) {
		if (cameras.length > 0) {
			scanner.start(cameras[1]);
		}
	}).catch(function (e) {
		console.error(e);
	});          
}

function cancelarform() {
	bootbox.alert("Seguro que desea cancelar?", function () {
		$(location).attr("href", "estado.php");
	});
}

function postergarfirma() {
	bootbox.confirm("Seguro que desea postergar la firma del cliente?", function(result){
		if (result) {
			$('.formFirmaTpl').toggle();
			$('#firmavali').prop('required', !$('#firmavali').prop('required'));
			$('#nomcli').prop('required', !$('#nomcli').prop('required'));
			$('#emailcli').prop('required', !$('#emailcli').prop('required'));
			$('#porfirmar').val($('#firmavali').prop('required'));
			$('#observaciones').focus();
			$('#btnPostergar').prop('disabled', 'disabled');
			// disabled="disabled"
		}
		// console.log('This was logged in the callback: ' + result); 
	});
	/*bootbox.alert("Seguro que desea postergar la firma del cliente?", function () {
		$('#formfirma2').toggle();
	});*/
}

function formnewinforme(idservicio, idascensor, tipoencuesta) {
	$.post("../ajax/servicio.php?op=modalperiodoinforme", function (data, status) {
		$("#contenido").html(data);
		$('#modalPreview').modal('show');

		$('body').on('click', '#modalPreview .modal-footer button', function(e) {
			var botonclick = $(this);
			if (botonclick.attr('id') == 'selperiodo') {
				var periodo = $("#periodo").val();
				// alert(periodo);
				$.post("../ajax/servicio.php?op=formnewinforme", {idservicio: idservicio, idascensor: idascensor, tipoencuesta: tipoencuesta, periodo: periodo}, function (data, status) {
					$("#otradata").html(data); //data es el html procesada de la plantilla
					$("#inicioservicio").hide();
					$("#finservicio").hide();
					$("#inicio").hide();
					$("#otradata").show();

					canvas = document.getElementById('firmafi');
					if (canvas) {
						canvas.height = canvas.offsetHeight;
						canvas.width = canvas.offsetWidth;
						signaturePad = new SignaturePad(canvas, {
							backgroundColor: 'rgb(255, 255, 255)',
							penColor: 'rgb(0, 0, 0)'
						});
					}
					return;
				});
			}else{
				return;
			};
		});

		// console.log(lco);
		// alert(periodo);

		/*$.post("../ajax/servicio.php?op=formnewinforme", {idservicio: idservicio, idascensor: idascensor, tipoencuesta: tipoencuesta}, function (data, status) {
			$("#otradata").html(data); //data es el html procesada de la plantilla

			$("#inicioservicio").hide();
			$("#finservicio").hide();
			$("#inicio").hide();
			$("#otradata").show();

			canvas = document.getElementById('firmafi');
			if (canvas) {
				canvas.height = canvas.offsetHeight;
				canvas.width = canvas.offsetWidth;
				signaturePad = new SignaturePad(canvas, {
					backgroundColor: 'rgb(255, 255, 255)',
					penColor: 'rgb(0, 0, 0)'
				});
			}
			return;
		});*/
	});
}

$('body').on('click', '#modalPreviewww .modal-footer button', function(e) {
	var botonclick = $(this);
	if (botonclick.attr('id') == 'selperiodo') {
		periodo = $("#periodo").val();
		return periodo;
	} else {
		periodo = '';
		return periodo;
	};
});

$('body').on('click', '#modalPPPPPreview .modal-footer button', function(e) {
	var botonclick = $(this);

	if (botonclick.attr('id') == 'selperiodo') {
		var periodo = $("#periodo").val();
		// alert(periodo);

		$.post("../ajax/servicio.php?op=formnewinforme", {idservicio: idservicio, idascensor: idascensor, tipoencuesta: tipoencuesta, periodo: periodo}, function (data, status) {
			$("#otradata").html(data); //data es el html procesada de la plantilla

			$("#inicioservicio").hide();
			$("#finservicio").hide();
			$("#inicio").hide();
			$("#otradata").show();

			canvas = document.getElementById('firmafi');
			if (canvas) {
				canvas.height = canvas.offsetHeight;
				canvas.width = canvas.offsetWidth;
				signaturePad = new SignaturePad(canvas, {
					backgroundColor: 'rgb(255, 255, 255)',
					penColor: 'rgb(0, 0, 0)'
				});
			}
			return;
		});
	} else {
		return;
	};
	// alert('hola');
});

function guardarinforme(e) {
	e.preventDefault();
	if (fijarfirma()) {
		$("#btnGuardar").prop("disabled", true);
		var formData = new FormData($("#formnewinforme")[0]);

		bootbox.confirm("¿Desea generar un nuevo informe para este servicio/equipo?", function (result) {
			if (result) {
				$.ajax({
					url: '../ajax/servicio.php?op=formguardarinforme',
					type: "POST",
					data: formData,
					contentType: false,
					processData: false,

					success: function (datos) {
						bootbox.alert(datos);
						mostarform(false);
						$("#inicioservicio").hide();
						$("#finservicio").hide();
						$("#otradata").hide();
						$("#inicio").show();
						$('#btnInforme').hide();
						$('#btnInforme2').show();
						$('#btnTerminar').prop('disabled', false);
					}
				});
			}
		});
		// limpiar();
	}
}

function MostrarPreview(tipo, id){
	if(tipo == 'informeservicio'){
		var url = '../ajax/servicio.php?op=PDFINFSERVICIO&visita='+id;
		var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
		$("#contenido").html(object);
	}else if(tipo == 'informeservicioescalera'){
		var url = '../ajax/servicio.php?op=PDFINFSERVICIO&tipo=escalera&visita='+id;
		var object = '<object class="PDFdoc" width="100%" style="height: 45vw;" data="'+ url +' "></object>';
		$("#contenido").html(object);
	}
	$('#modalPreview').modal('show');
}

function mensajeParadas() { //solicitado 20220310
    let minutosXparada = 5;
    let selectTipoServicio = document.getElementById("idtservicioso");
    let selectEquipo = document.getElementById("codigo");
    let valTipoServicio = selectTipoServicio.options[selectTipoServicio.selectedIndex].textContent;
    let valTipoEquipo = selectEquipo.options[selectEquipo.selectedIndex].getAttribute('tipoequipo');
    let valParadas = selectEquipo.options[selectEquipo.selectedIndex].getAttribute('paradas');
    let mensaje = '';
        
    if (valTipoServicio == 'MANTENCION' && valTipoEquipo == 'ASCENSOR') {
        mensaje = 'Estimado Técnico. Se informa que una vez iniciado el servicio de ' + valTipoServicio + ' del equipo ' + valTipoEquipo + ' seleccionado, dispone de un tiempo mínimo de <b style="color:blue">' + (valParadas * minutosXparada) + ' minutos</b> para poder cerrar la Guía de Servicio.<br><br>';
    }
    return mensaje;
}

init();